# Prohibited build from vehicle

Read this in another language | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Quick Links

[Changelog](CHANGELOG.md) | [Contributing](CONTRIBUTING.md)
|---|---|

## Contents

* [Overview](#overview)
* [Mod settings](#mod-settings)
    * [For map](#map)
* [Issues](#issue)
* [Features](#feature)
* [Installing](#installing)
* [License](#license)

## Overview

Players are forbidden build from vehicle.

## Mod settings

### <a name="map"></a> For maps:

* Allow to build railway system things
* Allow to build pipes and belts
* Allow to build walls
* Allow to build tiles
* Allow to build poles

## <a name="issue"></a> Found an Issue?

Please report any issues or a mistake in the documentation, you can help us by
[submitting an issue](https://gitlab.com/ZwerOxotnik/prohibited-build-from-vehicle/issues) to our GitLab Repository or on [mods.factorio.com](https://mods.factorio.com/mod/prohibited-build-from-vehicle/discussion).

## <a name="feature"></a> Want a Feature?

You can *request* a new feature by [submitting an issue](https://gitlab.com/ZwerOxotnik/prohibited-build-from-vehicle/issues) to our GitLab
Repository or on [mods.factorio.com](https://mods.factorio.com/mod/prohibited-build-from-vehicle/discussion).

## Installing

If you have downloaded a zip archive:

* simply place it in your mods directory.

For more information, see [Installing Mods on the Factorio wiki](https://wiki.factorio.com/index.php?title=Installing_Mods).

If you have downloaded the source archive (GitLab):

* copy the mod directory into your factorio mods directory
* rename the mod directory to prohibited-build-from-vehicle_*versionnumber*, where *versionnumber* is the version of the mod that you've downloaded (e.g., 2.0.0)

## License

```
MIT License

Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

[homepage]: http://mods.factorio.com/mod/prohibited-build-from-vehicle
[Factorio]: https://factorio.com/
