---------------------------------------------------------------------------------------------------
Version: 1.1.1
Date: 2019-01-06
  Features:
    - Added: settings for other things
---------------------------------------------------------------------------------------------------
Version: 1.1.0
Date: 2019-01-05
  Features:
    - Added: settings for railway system things, walls, belts, tiles, pipes, poles
    - Allowed to build in cheat mode
---------------------------------------------------------------------------------------------------
Version: 1.0.1
Date: 2019-01-04
  Features:
    - Allowed build a entity ghost from vehicle
---------------------------------------------------------------------------------------------------
Version: 1.0.0
Date: 2019-01-04
  Notes:
    - First release for 0.16
