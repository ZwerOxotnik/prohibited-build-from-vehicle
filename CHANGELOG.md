# Changelog

# 2019-01-06

### [v1.1.1][v1.1.1]

* Added: settings for other things

# 2019-01-05

### [v1.1.0][v1.1.0]

* Added: settings for railway system things, walls, belts, tiles, pipes, poles
* Allowed to build in cheat mode

## 2019-01-04

### [v1.0.1][v1.0.1]

* Changed: allowed build a entity ghost from vehicle

### [v1.0.0][v1.0.0]

* first release for 0.16

[v1.1.1]: https://mods.factorio.com/download/prohibited-build-from-vehicle/5c31aa95199eec000d2a53d2
[v1.1.0]: https://mods.factorio.com/download/prohibited-build-from-vehicle/5c30e358d366f0000c75d506
[v1.0.1]: https://mods.factorio.com/download/prohibited-build-from-vehicle/5c2fd58fd366f0000b695e30
[v1.0.0]: https://gitlab.com/ZwerOxotnik/prohibited-build-from-vehicle
