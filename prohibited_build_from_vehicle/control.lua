-- Prohibited build from vehicle
-- Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>
-- License: MIT
-- Version: 1.1.1 (2019.01.06)
-- Description: Players are forbidden build from vehicle.
-- Source: https://gitlab.com/ZwerOxotnik/prohibited-build-from-vehicle
-- Homepage: https://mods.factorio.com/mod/prohibited-build-from-vehicle

local mod = {}

local function is_allow_entity_type(type)
  if type == "transport-belt" or type == "underground-belt" or type == "splitter" or type == "pipe" or type == "pipe-to-ground" or type == "pump" then
    return settings.global["pbfv_allow_build_pipes_and_belts"].value
  elseif type == "gate" or type == "wall" then
    return settings.global["pbfv_allow_build_walls"].value
  elseif type == "electric-pole" then
    return settings.global["pbfv_allow_build_poles"].value
  elseif type == "rail-chain-signal" or type == "rail-signal" or type == "train-stop" or type == "straight-rail" or type == "curved-rail" then
    return settings.global["pbfv_allow_build_railway_system_things"].value
  else
    return settings.global["pbfv_allow_build_other"].value
  end
end

local function on_built_entity(event)
  -- Validation of data
  local player = game.players[event.player_index]
  if not (player and player.valid and player.character and player.vehicle) or player.cheat_mode then return end
  local created_entity = event.created_entity
  if not (created_entity and created_entity.type ~= "entity-ghost" and not is_allow_entity_type(created_entity.type)) then return end

  player.print({'prohibited_build_from_vehicle.warning'})
  player.mine_entity(created_entity, true)
end

local function on_player_built_tile(event)
  -- Validation of data
  local player = game.players[event.player_index]
  if not (player and player.valid and player.character and player.vehicle) or player.cheat_mode then return end
  if not (event.tiles and settings.global["pbfv_allow_build_tiles"].value and event.item.name ~= "landfill") then return end

  local surface = game.surfaces[event.surface_index]
  for _, tile in pairs(event.tiles) do
    player.mine_tile(surface.get_tile(tile.position))
  end
  player.print({'prohibited_build_from_vehicle.warning'})
end

mod.events = {
  on_built_entity = on_built_entity,
  on_player_built_tile = on_player_built_tile
}

return mod
