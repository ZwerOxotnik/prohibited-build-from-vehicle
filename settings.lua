data:extend({
	{
		type = "bool-setting",
		name = "pbfv_allow_build_railway_system_things",
		setting_type = "runtime-global",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "pbfv_allow_build_pipes_and_belts",
		setting_type = "runtime-global",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "pbfv_allow_build_walls",
		setting_type = "runtime-global",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "pbfv_allow_build_tiles",
		setting_type = "runtime-global",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "pbfv_allow_build_poles",
		setting_type = "runtime-global",
		default_value = true
	},
	{
		type = "bool-setting",
		name = "pbfv_allow_build_other",
		setting_type = "runtime-global",
		default_value = false
	}
})
