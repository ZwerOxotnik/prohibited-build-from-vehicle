# Запрет строительства из транспорта.

Хотите прочитать на другом языке? | [English](/README.md) | [Русский](/docs/ru/README.md)
|---|---|---|

## Быстрые ссылки

[Список изменений](CHANGELOG.md) | [Руководство контрибьютора](CONTRIBUTING.md)
|---|---|

## Содержание

* [Введение](#overview)
* [Сообщить об ошибки](#issue)
* [Настройки мода](#mod-settings)
    * [Для карты](#map)
* [Запросить функцию](#feature)
* [Установка](#installing)
* [Лицензия](#license)

## <a name="overview"></a> Введение

Игрокам запрещено строить из транспорта.

## <a name="mod-settings"></a> Настройки мода

### <a name="map"></a> For maps:

* Allow to build railway system things
* Allow to build pipes and belts
* Allow to build walls
* Allow to build tiles
* Allow to build poles

## <a name="issue"></a> Нашли ошибку?

Пожалуйста, сообщайте о любых проблемах или ошибках в документации, вы можете помочь нам
[submitting an issue](https://gitlab.com/ZwerOxotnik/prohibited-build-from-vehicle/issues) на нашем GitLab репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/prohibited-build-from-vehicle/discussion).

## <a name="feature"></a> Хотите новую функцию?

Вы можете *запросить* новую функцию [submitting an issue](https://gitlab.com/ZwerOxotnik/prohibited-build-from-vehicle/issues) на нашем GitLab репозитории или сообщите на [mods.factorio.com](https://mods.factorio.com/mod/prohibited-build-from-vehicle/discussion).

## Установка

Если вы скачали zip архив:

* просто поместите его в директорию модов.

Для большей информации, смотрите [вики Factorio "загрузка и установка модов"](https://wiki.factorio.com/Modding/ru#.D0.97.D0.B0.D0.B3.D1.80.D1.83.D0.B7.D0.BA.D0.B0_.D0.B8_.D1.83.D1.81.D1.82.D0.B0.D0.BD.D0.BE.D0.B2.D0.BA.D0.B0_.D0.BC.D0.BE.D0.B4.D0.BE.D0.B2).

Если вы скачали исходный архив (GitLab):

* скопируйте данный мод в директорию модов Factorio
* переименуйте данный мод в prohibited-build-from-vehicle_*версия*, где *версия* это версия мода, которую вы скачали (например, 2.0.0)

## Лицензия

Основная лицензия продукта - MIT
```
Copyright (c) 2019 ZwerOxotnik <zweroxotnik@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

[homepage]: http://mods.factorio.com/mod/prohibited-build-from-vehicle
[Factorio]: https://factorio.com/
