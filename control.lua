local pbft = require("prohibited_build_from_vehicle/control")

script.on_event(defines.events.on_built_entity, pbft.events.on_built_entity)
script.on_event(defines.events.on_player_built_tile, pbft.events.on_player_built_tile)
